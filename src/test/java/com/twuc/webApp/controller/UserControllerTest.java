package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.sources.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_user() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_use_function_Without_special_good() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("/api/segments/good"));
    }

    @Test
    void single_wildcard_using() throws Exception {
        mockMvc.perform(get("/api/dddx/hello"))
                .andExpect(status().isNotFound());
        mockMvc.perform(get("/api//hello"))
                .andExpect(status().isNotFound());
    }

//    @Test
//    void get_request_param() throws Exception {
//        mockMvc.perform(get("/api/students?gender=female&class=A"))
//                .andExpect(content().string("female A"));
//    }
//
//    @Test
//    void should_serialize_student() throws JsonProcessingException {
//        Student student = new Student();
//        student.setName("xiaoli");
//        student.setGender("female");
//        ObjectMapper objectMapper = new ObjectMapper();
//        String json = objectMapper.writeValueAsString(student);
//        assertEquals(json, "{\"name\":\"xiaoli\",\"gender\":\"female\"}");
//    }
//
//    @Test
//    void should_deserialize_student() throws IOException {
//        String json = "{\"name\":\"xiaoli\",\"gender\":\"female\"}";
//        ObjectMapper objectMapper = new ObjectMapper();
//        Student student = objectMapper.readValue(json, Student.class);
//        assertEquals("xiaoli", student.getName());
//    }
//
//    @Test
//    void should_create_student_and_return_name() throws Exception {
//        mockMvc.perform(post("/api/student")
//                .content("{\"name\":\"xiaoli\",\"gender\":\"female\"}")
//                .contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(content().string("xiaoli"));
//    }

    @Test
    void should_pattern_anything_using_wildcard_in_end() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(content().string("* pattern anything in the end"));
    }

    @Test
    void should_pattern_anything_using_wildcard_in_middle() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/middle/after"))
                .andExpect(content().string("* pattern anything in the middle"));
    }

    @Test
    void should_return_not_found_when_url_level_different() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_pattern_correct_url_using_prefix_and_suffix_wildcard() throws Exception {
        mockMvc.perform(get("/api/wildcard/prefix-aa"))
                .andExpect(content().string("* prefix is ok"));
        mockMvc.perform(get("/api/wildcard/bb-suffix"))
                .andExpect(content().string("* suffix is ok"));
    }

    @Test
    void should_pattern_correct_url_using_multiple_level_wildcard_in_middle() throws Exception {
        mockMvc.perform(get("/api/wildcard/level1/level2/level3"))
                .andExpect(content().string("** in middle is ok"));
    }

    @Test
    void should_pattern_correct_url_using_regex() throws Exception {
        mockMvc.perform(get("/api/wildcard/zpp/18"))
                .andExpect(content().string("zpp 18"));
    }

    @Test
    void should_return_request_param_using_postmapping() throws Exception {
        mockMvc.perform(post("/api/user")
                .param("username","zpp")
                .param("age","18"))
                .andExpect(content().string("zpp 18"));
    }

    @Test
    void should_return_request_param_using_postmapping_without_enough_param() throws Exception {
        mockMvc.perform(post("/api/user")
                .param("age","18"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_request_param_using_postmapping_without_extra_param() throws Exception {
        mockMvc.perform(post("/api/user")
                .param("gender","female"))
                .andExpect(status().isBadRequest());
    }
}