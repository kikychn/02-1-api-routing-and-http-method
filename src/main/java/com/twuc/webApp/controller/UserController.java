package com.twuc.webApp.controller;

import com.twuc.webApp.sources.Student;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/users/{userId}/books")
    public String getUserBooks(@PathVariable String userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/segments/good")
    public String getSegmentsGood() {
        return "/api/segments/good";
    }

    @GetMapping("/segments/{segmentName}")
    public String getSegmentsGoodWithParam(@PathVariable String segmentName) {
        return "/api/segments/" + segmentName;
    }

    @GetMapping("/?/hello")
    public String helloWorld() {
        return "hello world";
    }

//    @GetMapping("/students")
//    public String getGenderAndClass(@RequestParam String gender, @RequestParam("class") String klass) {
//        return gender + " " + klass;
//    }
//
//    @PostMapping("/student")
//    public String getStudentName(@RequestBody @Valid Student student) {
//        return student.getName();
//    }

    @GetMapping("/wildcards/*")
    public String getWildcardInEnd() {
        return "* pattern anything in the end";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getWildcardInMiddle() {
        return "* pattern anything in the middle";
    }

    @GetMapping("/wildcard/prefix*")
    public String getWildcardPrefix() {
        return "* prefix is ok";
    }

    @GetMapping("/wildcard/*suffix")
    public String getWildcardSuffix() {
        return "* suffix is ok";
    }

    @GetMapping("/wildcard/**/level3")
    public String getMutipleWildcard() {
        return "** in middle is ok";
    }

    @GetMapping("/wildcard/{username:[a-z]+}/{age:\\d{1,3}}")
    public String getRegexWildcard(@PathVariable String username, @PathVariable Integer age) {
        return username + " " + age;
    }

    @PostMapping(value = "/user", params = {"!gender"})
    public String getRequestParam(@RequestParam String username, @RequestParam Integer age) {
        return username + " " + age;
    }
}
